"""
Fairly dumb OpenVPN post-auth script.
If using LDAP to authenticate to OpenVPN, the user's LDAP group memberships will be checked.
Based on those groups OpenVPN's conn_groups will be applied to the connection. This is useful,
for example, when a group defines routes a group should have access to and the administrator needs
to centrally manage this access rather than being required to keep OpenVPN up to date.

Note the hard-coded group names, this can be cleaned up. Also note the use of isMemberOf rather
than memberOf. This will be dependent on the LDAP implementation.
I'm leaving in the print statements so any issues can be more easily spotted by tailing the
OpenVPN AS log.
"""

import re
import ldap

from pyovpn.plugin import *

# regex to parse the first component of an LDAP group DN
re_group = re.compile(r"^cn=([^,]+)")

# regex to parse the major component of a dotted version number
re_major_ver = re.compile(r"^(\d+)\.")


GROUP_SELECT = True


def ldap_groups_parse(res):
    ret = set()
    for g in res[0][1]['isMemberOf']:
        m = re.match(re_group, g)
        if m:
            ret.add(m.groups()[0])
    return ret


# Called by Access Server
def post_auth(authcred, attributes, authret, info):
    print "AUTHCRED: ", authcred
    print "ATTRIBUTES: ", attributes
    print "AUTHRET: ", authret
    print "INFO: ", info
    proplist = authret.setdefault('proplist', {})
    group = None

    if info.get('auth_method') == 'ldap':
        # get the user's distinguished name
        user_dn = info['user_dn']

        # use our given LDAP context to perform queries
        with info['ldap_context'] as ldap_context:
            # get the LDAP group settings for this user

            ldap_groups = ldap_groups_parse(ldap_context.search_ext_s(user_dn, ldap.SCOPE_SUBTREE, attrlist=["isMemberOf"]))
            print "********** LDAP_GROUPS", ldap_groups

            # determine the access server group based on LDAP group settings
            if 'eng' in ldap_groups:
                group = "eng"
            elif 'ops' in ldap_groups:
                group = "ops"
            elif 'prod' in ldap_groups:
                group = "prod"

        if group:
            proplist['conn_group'] = group

        print authret

    return authret
