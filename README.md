OpenVPN post-auth script for connecting ldap groups to openvpn groups allowing central group management in ldap.

Done as part of DevOps Days Austin's first Hackathon in 2016.